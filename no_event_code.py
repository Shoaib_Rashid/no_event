import pandas as pd
import datetime
from updateconfigchanges import sid_key_ttl_hit
import pandas_gbq as pg
import os
import argparse
import blacklister_new as bl


class No_Event_Occur():
    
    def __init__(self, now_time, year, month, day, hour):
        
        self.current_time = now_time
        self.current_year = year
        self.current_month = month
        self.current_day = day
        self.current_hour = hour
        self.config = {}
        self.ip_to_checked = []
        self.list_to_upload = []
        
    
    def update_config(self):
        
        self.config["sid"] = int(self.Api_id)
        self.config['hourly_ip_limit'] = 50
        self.config['captcha_limit'] =30
        self.config['ttl'] = self.blocked_time
        self.config['rule_failed'] = 'r2' 
        self.config['reason_code'] = 8
        self.config['action'] = 1
        self.config['calltype'] = 1
        self.config['type'] = 'ip'
        self.config['rcauto'] = True
        self.config['source'] = 'project_x'
        self.config['authkey'] = 'scghn'
        self.config['rule_name'] = 'ML/Event Rule'
        self.config['version'] = '1.0.0'
        self.config['description'] = 'Click based rule'
        self.config['rotom_listening_on'] = 1
        self.config['rotomredishost'] = '127.0.0.1'
        self.config['rotomredisport'] = '6379'
        self.config['rotomredispwd'] = 'redis@123@Azure'
        self.config['kafka_bootstrap_servers'] = 'kafka-broker-1:9092,kafka-broker-2:9092,kafka-broker-3:9092'
        self.config['kafka_prod_rotom_topic'] = 'blacklister'
#         print(self.config)
        
    def p_7(self, days):
        
        """
        get table of seven days
        
        """
        all_table = ''
        for i in range(0,days):
            
            previous_date = self.current_time - datetime.timedelta(i)
            previous_day = previous_date.strftime('%d')
            previous_month = previous_date.strftime('%m')
            previous_year = previous_date.strftime('%y')
            
            table_name = "{}_{}{}{}_%".format(self.Api_id,previous_day,previous_month, previous_year)
            all_table = all_table + '\n\tTABLE_QUERY([ss-production-storage.Citadel_Stream],\'table_id like "' + table_name + '"\'),'
        
        return all_table    
        
    def created_query(self,):
        query_file = open(cmd_args['query'],'r').read()
        sample_query = query_file.format(self.current_day,self.current_month,self.current_year,self.Api_id,self.c_id,self.hit,self.p_7(7))
        return sample_query
    
    def create_upload_list(self,df):
        
        if df.shape[0] != 0:
            
            previous_file = "{}_{}".format(self.c_id,int(self.current_day) - 1)
            if os.path.exists(previous_file):
                
                past_df = pd.read_csv(previous_file)
                self.ip_to_checked.append(past_df["0"])
                
            for num in df.index:
                if  self.key !=0 and df["IP"][num] not in self.ip_to_checked and df["IP"][num] not in self.list_to_upload:
                    self.list_to_upload.append(str(df["IP"][num]))
                    self.key = int(self.key) -1
                    
                if int(self.key) == 0:
                    break

        return self.list_to_upload
    
    def save_result_to_csv(self,df, file_name):
        
        list_to_upload = self.create_upload_list(df)
        # print(list_to_upload)
        if cmd_args['status']  == 1:
            # print('active_mode')
            bl.rotom_blacklister(list_to_upload,self.config) 
        
        df_ip = pd.DataFrame(list_to_upload)
        df_ip.to_csv(file_name,index=False)
        
        if not os.path.exists("{}_final.csv".format(self.c_id)):
            final_file_name = "{}_final.csv".format(self.c_id)
            df_ = pd.DataFrame(self.list_to_upload)
            df_.to_csv(final_file_name,index=False)
        else :
            df_ = pd.DataFrame(data=self.list_to_upload)
            with open("{}_final.csv".format(self.c_id), 'a') as f:
                df_.to_csv(f,header=False, index= False)
        
        
    
    def main_test(self, ):
        
        for i in range(len(sid_key_ttl_hit)):
            
            self.ip_to_checked = []
            self.upload_list = []
            
            self.c_id = sid_key_ttl_hit[i][0] # customer id
            self.key = sid_key_ttl_hit[i][1]
            self.ttl = sid_key_ttl_hit[i][2] # blocked for days
            self.hit = sid_key_ttl_hit[i][3] # No event at particular IP
            self.Api_id = sid_key_ttl_hit[i][4]# Api Id
            
            self.blocked_time = int(self.ttl) * 86400 # blocked time for IP  and 86400 is number of second in day 
            # print(self.c_id, self.key,self.ttl, self.hit, self.Api_id, self.blocked_time)
            
            self.update_config()
    
            # creating csv file
            file_name = "{0}_{1}.csv".format(self.c_id,self.current_day)
            query = self.created_query()
            df = pd.io.gbq.read_gbq(query, project_id="ss-production-storage", index_col=None,dialect='legacy', col_order=None, reauth=False, verbose=False, private_key=None)
            print(df.head())
            self.save_result_to_csv(df, file_name)
            
#             print(query)

            
if __name__ == '__main__':

    # command line argument
    parser = argparse.ArgumentParser()
    parser.add_argument('-qf', '--query', required = True, help = 'enter txt file name where query is written' )
    parser.add_argument('-s', '--status', type = int, default = 1, help='default value 1 for active mode and 0 of monitor mode')
    cmd_args  = vars(parser.parse_args())

    # getting current time
    current_time = datetime.datetime.utcnow()

    year = current_time.strftime('%y')
    month = current_time.strftime('%m')
    day = current_time.strftime('%d')
    hour = current_time.strftime('%H')

    # Creating object of no_event_Occur class 
    test1 = No_Event_Occur(current_time, year, month, day, hour)
    test1.main_test()
