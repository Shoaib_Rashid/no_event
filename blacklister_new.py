import time, sys
from datetime import datetime

from rotom_input_builder.rotom_input_builder import Builder
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

file_handler = logging.FileHandler('anomaly_blacklister.log')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)






'''
blacklist all the ips in ip_list
'''
def rotom_blacklister(ip_list, config):
    #config = anomaly_config.configure()
    config = config
    hourly_ip_limit = config['hourly_ip_limit']
    hourly_ip_count = len(ip_list)
    logger.info('hourly ip limit ={x} and hourly ip count ={y}'.format(x=hourly_ip_limit,y=hourly_ip_count))

    if (hourly_ip_count > hourly_ip_limit):
        logger.error( 'number of ips are greater than hourly ip limit')
        sys.exit(1)


    '''
    Creating sender instance for producing over kafka
    '''
    try:
        sender = RotomSender(config)
    except Exception as e:
        logger.error('error in blacklister rotom ', exc_info=True)
        sys.exit(1)




    '''
    create json for each ip in ip_list
    and send it to kafka/redis topic/list
    '''
    for ip in ip_list:
        builder = Builder()

        try:
            iplist = list()
            iplist.append(str(ip))

            key_list = list()
            key_dict = dict()
            key_dict["field"] = '_zpsbd6'
            key_dict["value"] = str(ip)
            key_dict["rc"] = str(config['rule_failed'])
            key_dict["rule"] = str(config['rule_name'])
            key_dict["ttl"] = int(config['ttl'])
            key_list.append(key_dict)

            builder.add_sid(str(config['sid']))
            builder.add_action(int(config['action']))
            builder.add_calltype(int(config['calltype']))
            builder.add_iplist(iplist)
            builder.add_type(str(config['type']))
            builder.add_rcauto(bool(config['rcauto']))
            builder.add_keylist(key_list)
            builder.add_ualist(list())
            builder.add_source(str(config['source']))
            builder.add_version(str(config['version']))
            builder.add_description(str(config['description']))
            builder.add_timestamp(long(time.time()*1000))
            builder.add_date(datetime.utcnow().strftime("%d%m%y"))
            builder.add_hour(datetime.utcnow().strftime("%H"))
            builder.add_reasoncode(int(config['reason_code']))
            builder.add_authkey(str(config['authkey']))
            complete, e = builder.is_rotom_input_complete()

            if not complete:
                logger.info('rotom_packet_incomplete:{}'.format(str(e)))
                continue

            packet = builder.to_json()
            sender.send_to_rotom(packet)
            logger.info('packet_send_rotom:{}'.format(str(packet)))

        except Exception as e:
            logger.error('error in blacklister  ', exc_info=True)
            sys.exit(1)
            pass
        finally:
            del builder

    sender.kafka_flush()
    time.sleep(2)



from kafka import KafkaProducer
import redis

class RotomSender():

    '''
    initialise kafka/ redis based on rotom configuration
    '''
    def __init__(self, config):
        self.config = config
        if self.config['rotom_listening_on'] == 1:
            self.conn = KafkaProducer(bootstrap_servers = self.config['kafka_bootstrap_servers'], \
                                      acks = 0, retries = 0, batch_size = 16384, linger_ms = 1, \
                                      buffer_memory = 33554432, max_block_ms = 60000, api_version = (0,10))
        elif self.config['rotom_listening_on'] == 2:
            self.conn = redis.ConnectionPool(host = self.config['rotomredishost'], port = self.config['rotomredisport'], password = self.config['rotomredispwd'], max_connections = 40)

    '''
    send the packet to rotom for analysis and key creation
    '''
    def send_to_rotom(self, packet):
        if self.config['rotom_listening_on'] == 1:
            self.conn.send(self.config['kafka_prod_rotom_topic'], packet)#.get(timeout = 30)
        elif self.config['rotom_listening_on'] == 2:
            redisins = redis.Redis(connection_pool = self.conn)
            redisins.lpush(self.config['kafka_prod_rotom_topic'], packet)
            del redisins

    '''
    As using Async Kafka Producer, flush all the messages from buffer before program exiting
    Note: if running as a Kube Cron please give some sleep before the kube exits.
    '''
    def kafka_flush(self):
        if self.config['rotom_listening_on'] == 1:
            self.conn.flush()